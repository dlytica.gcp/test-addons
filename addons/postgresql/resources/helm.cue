output: {
	name: "addon-postgresql"
	type: "helm"
	properties: {
		repoType: "helm"
		url:      "https://charts.bitnami.com/bitnami"
		chart:    "postgresql"
		version:  "15.2.5"
	}
}