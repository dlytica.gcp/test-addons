output: {
	name: "addon-pgadmin4"
	type: "helm"
	properties: {
		repoType: "helm"
		url:      "https://helm.runix.net/"
		chart:    "pgadmin4"
		version:  "1.23.3"
	}
}